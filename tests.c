#include <stdio.h>
#include "CUnit.h"
#include "Basic.h"
#include "code.h"

static double epsilon = 0.000001;

void runTest1(int,int,int,double,double);
void runTest2(int,int,int,double,double);
void runTest3(int,int,int,double,double);
void runTest4(int,int,int,double,double);

//This is just a prototype
void runTest1a(int,int,int,double,double);
void runTest2a(int,int,int,double,double);

/* The actual unit tests.
 * Each of the unit test functions calls a helper function
 * to set up and perform the test (see below).
 */

void test11(void) { runTest1(2,3,5,1,10); }
void test12(void) { runTest2(2,3,5,1,10); }
void test13(void) { runTest3(2,3,5,1,10); }
void test14(void) { runTest4(2,3,5,1,10); }

//My own function
//We place in our coefficients and the value of x. Equal to 14. 
void test15(void) { runTest1a(3,5,6,1,14); }
void test16(void) {runTest2a(1,1,1,1,3);}
void test17(void) {runTest3(2,3,5,1,14);}
void test18(void) {runTest4(2,3,5,1,22);}
           


/* The helper functions.
 *
 * Given values for the coefficients a, b, and c,
 * a specific value for x,
 * and an expected value for the polynomial at x, 
 * each of the following helper functions sets up
 * and runs a test for one of the functions polyEval1, 
 * polyEval2,  polyEval3, or  polyEval4.
 *
 * They are helper functions that make writing tests for the
 * functionality of polyEval functions easier.  For example,
 * see the test11 function above that only calls runTest1 with
 * appropriate values for a, b, c, x, and expected.
 */

void runTest1(int a, int b, int c, double x, double expected) {
  double actual = polyEval1(a,b,c,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}


void runTest1a(int a, int b, int c, double x, double expected) {
  double actual = polyEval1A(a,b,c,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}



void runTest2(int a, int b, int c, double x, double expected) {
  double actual = polyEval2(a,b,c,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}


void runTest2a(int a, int b, int c, double x, double expected) {
  double actual = polyEval2a(a,b,c,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}



void runTest3(int a, int b, int c, double x, double expected) {
  int coeff[] = {a,b,c};
  double actual = polyEval3(coeff,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}

void runTest4(int a, int b, int c, double x, double expected) {
  int coeff[] = {a,b,c};
  double actual = polyEval4(coeff,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}




/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if (
          (NULL == CU_add_test(pSuite, "test11", test11))
       || (NULL == CU_add_test(pSuite, "test12", test12)) 
       || (NULL == CU_add_test(pSuite, "test13", test13)) 
       || (NULL == CU_add_test(pSuite, "test14", test14)) 
       || (NULL) == CU_add_test(pSuite, "test15", test15)
        || (NULL) == CU_add_test(pSuite, "test16", test16)
        || (NULL) == CU_add_test(pSuite, "test17", test17)
         || (NULL) == CU_add_test(pSuite, "test18", test18)

      )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   //   CU_basic_show_failures(CU_get_failure_list());
   CU_cleanup_registry();
   return CU_get_error();
}
