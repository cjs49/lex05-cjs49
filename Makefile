OBJECTS = code.o

TIMBERLAKE_PATH_PREFIX = /util/CUnit/
TIMBERLAKE_CUNIT_DIRECTORY = CUnit/
BREW_PATH_PREFIX = /usr/local/Cellar/cunit/2.1-3/
BREW_CUNIT_DIRECTORY = cunit

CUNIT_PATH_PREFIX = $(TIMBERLAKE_PATH_PREFIX)
CUNIT_DIRECTORY = $(TIMBERLAKE_CUNIT_DIRECTORY)

CC = gcc
NO_DEBUG_FLAGS = -c -Wall -std=c11
FLAGS = $(NO_DEBUG_FLAGS)

code.o: code.h
	$(CC) $(FLAGS) code.c

tests: $(OBJECTS) code.c
	gcc -Wall -L $(CUNIT_PATH_PREFIX)lib -I $(CUNIT_PATH_PREFIX)include/$(CUNIT_DIRECTORY) -o tests $(OBJECTS) tests.c -lcunit


clean:
	rm -f *~ *.o a.out code tests
