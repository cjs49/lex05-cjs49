double polyEval1(int a, int b, int c, double x) {
  double x2 = x*x;
  return a * x2 + b * x + c;
}

double polyEval1A(int a, int b, int c, double x) {
  double x2 = x*x;
  return a * x2 + b * x + c;
}


double polyEval2(int a, int b, int c, double x) {
  return (((a) * x + b) * x + c);
}

double polyEval2a(int a, int b, int c, double x) {
  return (((a) * x + b) * x + c);
}




double polyEval3(int coefficients[], double x) {
  double value = 0;
  for (int i=0; i<3; i++) {
    value = value * x + coefficients[i];
  }
  return value;
}

double polyEval4(int coefficients[], double x) {
  double value = 0;
  for (int i=2; i>=0; i--) {
    value = value * x + coefficients[i];
  }
  return value;
}
